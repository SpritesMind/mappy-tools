include makerules 

SRCS = $(wildcard $(SRCDIR)$(SEP)*.c)
SRCS += $(wildcard $(SRCDIR)$(SEP)utils$(SEP)*.c)
SRCS += $(wildcard $(ARGTABLE3)$(SEP)*.c)
OBJS = $(patsubst %.c,%.o,$(SRCS))

OBJ_DEBUG = $(patsubst $(SRCDIR)$(SEP)%,$(OBJDIR_DEBUG)$(SEP)%,$(OBJS))
OBJ_RELEASE =  $(patsubst $(SRCDIR)$(SEP)%,$(OBJDIR_RELEASE)$(SEP)%,$(OBJS))

all: release

ee:
	-$(ECHO) $(OBJS)


debug: libs folders $(OBJ_DEBUG) $(DEP_DEBUG) 
	$(LD) -o $(OUT_DEBUG) $(OBJ_DEBUG) $(LDFLAGS_DEBUG) $(LIBDIR_DEBUG) $(LIB_DEBUG) 

$(OBJDIR_DEBUG)$(SEP)%.o: $(SRCDIR)$(SEP)%.c
	$(CC) $(CFLAGS_DEBUG) $(INC_DEBUG) -c -o $@ $<

	
release: libs folders $(OBJ_RELEASE) $(DEP_RELEASE)
	$(LD) -o $(OUT_RELEASE) $(OBJ_RELEASE) $(LDFLAGS_RELEASE) $(LIBDIR_RELEASE)  $(LIB_RELEASE)

$(OBJDIR_RELEASE)$(SEP)%.o: $(SRCDIR)$(SEP)%.c
	$(CC) $(CFLAGS_RELEASE) $(INC_RELEASE) -c -o $@ $<


libs:  $(MAPPYLIB)$(SEP)libmappy.a $(ARGTABLE3)$(SEP)LICENSE

#will create libmappy and libmappyd at the same time
$(MAPPYLIB)$(SEP)libmappy.a: $(MAPPYLIB)$(SEP)Makefile
	$(MAKE) -C $(MAPPYLIB)

$(MAPPYLIB)$(SEP)Makefile:
	$(error Mappy library missing, please update git submodules)	

$(ARGTABLE3)$(SEP)LICENSE:
	$(error Argtable 3 library missing, please update git submodules)	

clean: clean_debug clean_release
	$(MAKE) -C $(MAPPYLIB) clean
	
clean_debug: 
	@echo Clean debug files and folders
	-$(RM) $(OBJ_DEBUG) $(HIDE_ERROR)
	-$(RM) $(OUT_DEBUG) $(HIDE_ERROR)
	-$(RMD) $(OBJDIR_DEBUG) $(HIDE_ERROR)

clean_release: 
	@echo Clean release files and folders
	-$(RM) $(OBJ_RELEASE) $(HIDE_ERROR)
	-$(RM) $(OUT_RELEASE) $(HIDE_ERROR)
	-$(RMD) $(OBJDIR_RELEASE) $(HIDE_ERROR)

folders: $(OBJDIR) $(OBJDIR_DEBUG) $(OBJDIR_RELEASE) 

$(OBJDIR):
	$(MKDIR) $@
	
$(OBJDIR_DEBUG):
	$(MKDIR) $@
	$(MKDIR) $@$(SEP)utils

$(OBJDIR_RELEASE):
	$(MKDIR) $@
	$(MKDIR) $@$(SEP)utils


	
.PHONY: all debug release clean clean_debug clean_release folders
