#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <math.h>
#include <argtable3.h>
#include <mappy.h>

#define FREEIMAGE_LIB
#include <FreeImage.h>

#include "utils\file_utils.h"
#include "utils\common.h"

//thanks StackOverflow ;)
#define min(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })

struct arg_lit	*help, *allowN;
struct arg_file *input, *file, *output;
struct arg_str	*format, *chunk;
struct arg_int	*blockStart, *blockCount;
struct arg_end	*end;

static void importChunk(const char *chunkName, const char *filename)
{
	unsigned char *chunkData;
	long fileSize;
	
	fileSize = file_getSize( (char *)filename );
	if (fileSize == 0L)
	{
		printf("error : Invalid source filename %s.\n", filename);
		return;
	}
	
	chunkData = malloc(fileSize);
	if (chunkData == NULL) {
		printf("Not enough memory to load %s.\n", filename);
		return;
    }
	
	FILE *source = fopen(filename,"rb");	
	fread(chunkData, fileSize, 1, source);
	fclose(source);

	mappy_setChunk((char *)chunkName, fileSize, chunkData);
		
	//do not free(chunkData); !!
}
static void importBitmap(const char *chunkName, HDRSTR *header, short int colordepth , const char *filename, int index, int maxBlock)
{
	unsigned char *chunkData;
	int blockToImport;
	FIBITMAP *bitmap = FreeImage_Load(FIF_BMP, filename, BMP_DEFAULT);
	if (bitmap == NULL)
	{
		printf("error : Unable to load %s\n", filename);
		return;
	}
		
	int width = FreeImage_GetWidth(bitmap);
	int height = FreeImage_GetHeight(bitmap);
	int scan_width = FreeImage_GetPitch(bitmap);

	if (FreeImage_GetBPP(bitmap) != colordepth)
	{
		printf("error : %s depth should be %dbpp, not %dbpp\n", filename, colordepth, header->mapdepth);
		FreeImage_Unload(bitmap);
		return;
	}
	if (width != header->mapblockwidth)
	{
		printf("error : %s width should be %d pixels\n", filename, header->mapblockwidth);
		FreeImage_Unload(bitmap);
		return;
	}
	if ( (height % header->mapblockheight) != 0 )
	{
		printf("error : %s height should be even multiple of %d\n", filename, header->mapblockheight);
		FreeImage_Unload(bitmap);
		return;
	}
	
	
	blockToImport = (width / header->mapblockwidth) * (height / header->mapblockheight);
	
	if (maxBlock != 0)	blockToImport = min(blockToImport,maxBlock);
	
	if ((index + blockToImport) > header->mapnumblockgfx ) 
	{
		blockToImport = header->mapnumblockgfx - index;
	}
	
	if ((maxBlock != 0) && (blockToImport != maxBlock))
		printf("warning : could only replace blocks %d to %d, not to %d\n", index, index+blockToImport, index+maxBlock);
	

	chunkData = malloc(height * scan_width);
	if (chunkData == NULL) {
		printf("Not enough memory to load %s.\n", filename);
		FreeImage_Unload(bitmap);
		return;
    }
	
	FreeImage_ConvertToRawBits(chunkData, bitmap, scan_width, colordepth, FI_RGBA_RED_MASK, FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK,TRUE);
	FreeImage_Unload(bitmap);
	
	if (index || maxBlock)
	{
		int blockWeight = header->mapblockheight * scan_width;
		CHUNK *rawChunk = mappy_getChunk( (char *)chunkName );
		if ( rawChunk == NULL )
		{
			printf("error : to edit chunk %s, it must already exist.\n",chunkName);
			return;
		}
		if ( rawChunk == NULL )
		{
			printf("error : to edit chunk %s, it must already exist.\n",chunkName);
			return;
		}
		//replace <blockToImport> blocks at <index>
		memcpy( rawChunk->data + (index * blockWeight), chunkData, blockToImport * blockWeight);
		
		rawChunk = NULL;
		free(chunkData);
	}
	else
	{
		//add it or rewrite it from scratch, easier
		mappy_setChunk((char *)chunkName, height * scan_width, chunkData);
		//do not free(chunkData); !!
	}
	
}

static void importCMAP(const char *filename)
{
	FIBITMAP *bitmap = FreeImage_Load(FIF_BMP, filename, BMP_DEFAULT);
	if (bitmap == NULL)
	{
		printf("error : Unable to load %s\n", filename);
	}
	
	RGBQUAD *pal = FreeImage_GetPalette(bitmap);
	if ( pal != NULL )
	{
		int i;
		int nbColors = min(256, FreeImage_GetColorsUsed(bitmap));
		unsigned char *chunkData = calloc(256, 3);
		if (chunkData == NULL)
		{
			printf("error : not enough memory\n");
			FreeImage_Unload(bitmap);
			return;
		}
		
		for (i = 0; i < nbColors; i++) {
			chunkData[i*3 + 0] = pal[i].rgbRed;
			chunkData[i*3 + 1] = pal[i].rgbGreen;
			chunkData[i*3 + 2] = pal[i].rgbBlue;
		}
		
		mappy_setChunk( "CMAP", 256*3, chunkData);
	
		//do not free(chunkData); !!!
	}
	
	FreeImage_Unload(bitmap);
}

int import(int argc, char *argv[])
{	
	int exitcode = 0;
	const char *outputfilename;
	int err;
	
	void *argtable[] = {
        help   = arg_litn(NULL, "help", 0, 1, "display this help"),
        input  = arg_file1("i", "input", "<infile>","input .FMP file"),
        file   = arg_file1("s", "source", "<file>","file to import"),
        output = arg_file0("o", "output", "<outfile>","output .FMP file (overwrite input if not specified)"),
		chunk  = arg_str1(NULL, NULL,"<chunk_name>", "FMP chunk to replace"),
		format = arg_strn("f", "format","<format>", 0, 1, "file format (RAW|BMP)" ),
        allowN = arg_litn(NULL, "allow-new", 0, 1, "allow new chunk"),
        blockStart  = arg_int0(NULL, "from", "<index>","first block to overwrite (BMP only, default 0)"),
        blockCount  = arg_int0(NULL, "count","<value>","number of blocks to import (BMP only, default 0)"),
        end    = arg_end(ERROR_BUFFER_SIZE),
    };
	
	
	FreeImage_Initialise(TRUE);
		
	if (arg_nullcheck(argtable) != 0)
	{
		printf("error: insufficient memory\n");
		exitcode = -1;
		goto exit;
    }

	//default values
	format->sval[0]="RAW";
	blockStart->ival[0]=0;
	blockCount->ival[0]=0;

	
    err = arg_parse(argc,argv,argtable);

    // special case: '--help' takes precedence over error reporting
    if (help->count > 0)
    {
        printf("Usage:\nIMPORT");
        arg_print_syntax(stdout, argtable, "\n");
        arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		
        printf("\nValid chunks:\n");
        printf("  MPHD\timport updated header (formats : RAW)\n");
        printf("  ATHR\timport updated author info (formats : RAW)\n");
        printf("  CMAP\timport updated color map (formats : RAW, BMP)\n");
        printf("  BGFX\timport updated tiles (formats : RAW, BMP)\n");
        printf("  AGFX\timport updated alternate tiles (formats : RAW, BMP)\n");
        printf("  BKDT\timport updated block info(formats : RAW)\n");
        printf("  ANDT\timport updated anim info(formats : RAW)\n");
        printf("  BODY\timport updated uncompressed layer 0 (formats : RAW)\n");
        printf("  LAYx\timport updated uncompressed layer (1 to 7) (formats : RAW)\n");
        //printf("  NLAY\textract layer x (formats : RAW)\n");
        printf("  xxxx\timport raw chunk, existing or not (format : RAW)\n");
		
		exitcode = 0;
		goto exit;
    }
	
	
	// If the parser returned any errors then display them and exit
    if ((err > 0) || (argc == 1))
    {
        // print upto ERROR_BUFFER_SIZE error details contained in the arg_end struct
        arg_print_errors(stdout, end, "error");
        printf("Try 'IMPORT --help' for more information.\n");
		exitcode = -1;
		goto exit;
    }

	
	err = mappy_load( (char *)input->filename[0]);
	if (err != MER_NONE)	
	{
		err = mappy_getLastErrorID();
		if (err == MER_NOOPEN)
			printf("File not found : %s\n", (char *)input->filename[0]);
		else if (err == MER_OUTOFMEM)
			printf("Not enough memory to load %s\n", (char *)input->filename[0]);
		else //if (err == MER_MAPLOADERROR)
			printf("Error %d loading %s\n", err, (char *)input->filename[0]);
		exitcode = -1;
		goto exit;
	}
	
	if (allowN->count ==0)
	{
		CHUNK *rawChunk ;
		rawChunk = mappy_getChunk( (char *)chunk->sval[0] );
		if (rawChunk == NULL)
		{
			printf("error : chunk %s not found.\n",chunk->sval[0]);
			exitcode = -1;
			goto exit;
		}
		rawChunk = NULL;
	}
	
	if (format->count == 0)
	{
		//TODO
		//check bmp extension
	}
	
	if (strcmp("RAW", format->sval[0]) == 0)
	{
		if (blockStart->count)	printf("warning : --from inactive on RAW mode\n");
		if (blockCount->count)	printf("warning : --count inactive on RAW mode\n");
		importChunk(chunk->sval[0], file->filename[0]);
	}
	else if (strcmp("BMP", format->sval[0]) == 0)
	{
		HDRSTR *header = mappy_getHeader();
		if (header == NULL)
		{
			printf("error : header not found.\n");
			exitcode = -1;
			goto exit;
		}
			
		if (strcmp("BGFX", chunk->sval[0]) == 0)
		{
			importBitmap("BGFX", header, header->mapdepth, file->filename[0], blockStart->ival[0], blockCount->ival[0]);
		}
		else if (strcmp("AGFX", chunk->sval[0]) == 0)
		{
			importBitmap("AGFX", header, 8, file->filename[0], blockStart->ival[0], blockCount->ival[0]);
		}
		else if (strcmp("CMAP", chunk->sval[0]) == 0)
		{
			importCMAP(file->filename[0]);
		}
		else
		{
			printf("error : Can't extract chunk %s to bitmap.\n", chunk->sval[0]);
			goto exit;
		}
	}
	else
	{
		printf("error : Invalid format %s for chunk %s.\n", format->sval[0], chunk->sval[0]);
		goto exit;
	}
	
	outputfilename = input->filename[0];
	if (output->count)	outputfilename = output->filename[0];

	err = mappy_save((char *)outputfilename);
	if (err != 0)	
	{
		err = mappy_getLastErrorID();
		if (err == MER_NOOPEN)
			printf("Unable to write to  %s\n", (char *)outputfilename);
		else if (err == MER_OUTOFMEM)
			printf("Not enough memory");
		else //if (err == MER_MAPLOADERROR)
			printf("Error %d writing to %s\n", err, (char *)outputfilename);
		exitcode = -1;
		goto exit;
	}
	
	exitcode = 0;
	
exit:
	FreeImage_DeInitialise();
	mappy_close();
	
    // deallocate each non-null entry in argtable[] 
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));

    return exitcode;
}