#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <mappy.h>
#include <argtable3.h>

#include "utils\file_utils.h"
#include "utils\common.h"


	 
struct arg_lit *help; //, *duplicate, *extract;
struct arg_file *input, *output;
struct arg_end *end;

static signed short int *mappingTable;
static HDRSTR *header;
static unsigned char *gfx;
static BLKSTR *blocks;
static int duplicateCount;

static void updateLayers(int newBlockID, int oldBlockID)
{
	signed short int *layer;
	short int blockID;
	int i,j,k;
	for (k = 0; k < 8; k++)
	{
		layer = mappy_getLayer(k);
		if (layer != NULL)
		{
			for (i = 0; i < header->mapheight; i++)
			{
				for (j = 0; j< header->mapwidth; j++)
				{
					blockID = layer[ j + header->mapwidth*i ];
					if (blockID == oldBlockID)
						layer[ j + header->mapwidth*i ] = newBlockID;
					else if (blockID > oldBlockID)
						layer[ j + header->mapwidth*i ] = blockID -1;
				}
			}
		}
		else
			break;
	}
}

static void updateBlocks(int newGFXID, int oldGFXID)
{
	int i;
	for (i = 0; i < header->mapnumblockstr; i++)
	{
		BLKSTR* block =&blocks[i];
		if (block->bgoff == oldGFXID )
		{
			block->bgoff = newGFXID;
		}
		else if (block->bgoff > oldGFXID )
		{
			block->bgoff--;
		}
	}
}
static void removeBlock(int blockID)
{
	int blockSize = sizeof(BLKSTR);
	BLKSTR* newBlocks = calloc((header->mapnumblockstr-1), blockSize);
	if (newBlocks == NULL)
	{
		printf("error : Not enough memory to adjust blocks\n");
		return;
	}
	
	memcpy(newBlocks, blocks, blockSize*blockID);
	memcpy(newBlocks+(blockSize*blockID), blocks+(blockSize*(blockID+1)), blockSize*(header->mapnumblockstr-blockID-1));

	header->mapnumblockstr--; //to be set BEFORE mappy_setBlocks
	mappy_setBlocks(newBlocks);
	blocks = newBlocks;
}

static void removeGFX(int gfxID)
{
	int gfxSize = mappy_getBlockGFXSize();
	unsigned char * newGFX = calloc((header->mapnumblockgfx-1),gfxSize);
	if (newGFX == NULL)
	{
		printf("error : Not enough memory to adjust gfx\n");
		return;
	}
	
	memcpy(newGFX, gfx, gfxSize*gfxID);
	memcpy(newGFX+(gfxSize*gfxID), gfx+(gfxSize*gfxID+1), gfxSize*(header->mapnumblockgfx-gfxID-1));

	header->mapnumblockgfx--; //to be set BEFORE mappy_setBlockGFX
	mappy_setBlockGFX(newGFX);
	gfx = newGFX;
}
static int getBlock(int gfxIdx)
{
	int i;
	for (i = 0; i < header->mapnumblockstr; i++)
	{
		BLKSTR* block =&blocks[i];
		if (block->bgoff == gfxIdx )
		{
			return i;
		}
	}
	
	return -1;
}

static int getDuplicate(unsigned char *tile, int index )
{
	unsigned int tileIdx, i, j;
	unsigned char same = TRUE;
	//WARNING : works only with paletized bitmap
	for (tileIdx = 0; tileIdx < index-1; tileIdx++)
	{
		same = TRUE;
		for(i = 0; i < header->mapblockheight; i++)
		{
			unsigned char *bits_a = tile + i*header->mapblockwidth;
			unsigned char *bits_b = (gfx + tileIdx*mappy_getBlockGFXSize() + i*header->mapblockwidth);

			for (j = 0; j < header->mapblockwidth; j++)
			{
				if (bits_a[j] != bits_b[j]) 
				{
					same = FALSE;
					break;
				}
			}
			if (!same)	break;
		}
		
			
		if (same)
		{
			duplicateCount++;
			return tileIdx;
		}
	}
	
	return index;
}

static void run( )
{
/****
	if gfx is a duplicate
		find block using original
		find block using duplicate
		update layerS
			use original block instead of duplicate
			use block id-- if block id > block duplicate id
		update every block using a tile > duplicate (tileIdx--)
		remove block using duplicate
		remove duplicate gfx
		update header->numblockgfx--/str--
*****/

	int i;
	int firstcopy, firstCopyBlock;
	int duplicate, duplicateBlock;
	
	duplicateCount = 0;
	for (i= header->mapnumblockgfx-1; i > 1; i--)
	{
		firstcopy = getDuplicate(gfx + i*mappy_getBlockGFXSize(), i );
		if (firstcopy != i)
		{
			duplicate = i;
			duplicateBlock = getBlock(duplicate);
			firstCopyBlock = getBlock(firstcopy);

			updateLayers(firstCopyBlock, duplicateBlock);
			updateBlocks(firstcopy, duplicate);
			removeBlock(duplicateBlock);
			removeGFX(duplicateBlock);
		}
	}
	
}



int optimize(int argc, char *argv[])
{
	int exitcode = 0;
	int err, i;
	
	void *argtable[] = {
        help    = arg_litn(NULL, "help", 0, 1, "display this help"),
        input 	= arg_file1("i", "input", "<infile>","input .FMP file"),
        output  = arg_file0("o", "output", "<outfile>","output .FMP 1.0 file"),
        end     = arg_end(ERROR_BUFFER_SIZE),
    };
	
	if (arg_nullcheck(argtable) != 0)
	{
		printf("error: not enough memory\n");
		exitcode = -1;
		goto exit;
    }	
	
    err = arg_parse(argc,argv,argtable);

    // special case: '--help' takes precedence over error reporting
    if (help->count > 0)
    {
        printf("Usage:\nOPTIMIZE");
        arg_print_syntax(stdout, argtable, "\n");
        arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		
        printf("\nWarning : if no output is defined, input will be overwritten\n");
		exitcode = 0;
		goto exit;
    }
	
	
	// If the parser returned any errors then display them and exit
    if ((err > 0) || (argc == 1))
    {
        // print upto ERROR_BUFFER_SIZE error details contained in the arg_end struct
        arg_print_errors(stdout, end, "error");
        printf("Try 'OPTIMIZE --help' for more information.\n");
		exitcode = -1;
		goto exit;
    }
	
	if (output->count == 0)	
	{
		output->filename[0] = calloc(FILENAME_MAX, 1);
		strcpy((char *)output->filename[0], input->filename[0]);
		
	}
	
	if (!file_isValid( (char *)input->filename[0]))
	{
		printf("error : map %s not found", input->filename[0]);
		exitcode = -1;
		goto exit;
	}
	
	
	err = mappy_load( (char *)input->filename[0]);
	if (err != MER_NONE)	
	{
		err = mappy_getLastErrorID();
		if (err == MER_NOOPEN)
			printf("File not found : %s\n", (char *)input->filename[0]);
		else if (err == MER_OUTOFMEM)
			printf("Not enough memory to load %s\n", (char *)input->filename[0]);
		else //if (err == MER_MAPLOADERROR)
			printf("Error %d loading %s\n", err, (char *)input->filename[0]);
		exitcode = -1;
		goto exit;
	}
	
	header = mappy_getHeader();
	if ((header == NULL) || header->mapdepth != 8)
{ 
		printf("Only 8bits maps are supported yet\n");
		exitcode = -1;
		goto exit;
	}
	gfx = mappy_getBlockGFX();
	if ( (gfx == NULL) || (header->mapnumblockgfx == 0) )
	{ 
		printf("error : no GFX to optimize\n");
		exitcode = -1;
		goto exit;
	}
	
	blocks = mappy_getBlocks();
	if ( (blocks == NULL) || (header->mapnumblockstr == 0) )
	{ 
		printf("error : no block to optimize\n");
		exitcode = -1;
		goto exit;
	}
	
	mappingTable = calloc(header->mapnumblockgfx, sizeof(signed short int));
	if (mappingTable == NULL)
	{
		printf("error: not enough memory\n");
		exitcode = -1;
		goto exit;
	}
	for (i=0; i < header->mapnumblockgfx; i++)
	{
		mappingTable[i] = i;
	}
	
	run();
	
	if (duplicateCount != 0)
	{
		printf("info: %d duplicate removed\n",duplicateCount);
	}
		
	err = mappy_save((char *) output->filename[0]);
	if (err != 0)	
	{
		err = mappy_getLastErrorID();
		if (err == MER_NOOPEN)
			printf("Unable to write to  %s\n", (char *) output->filename[0]);
		else if (err == MER_OUTOFMEM)
			printf("Not enough memory");
		else //if (err == MER_MAPLOADERROR)
			printf("Error %d writing to %s\n", err, (char *) output->filename[0]);
		exitcode = -1;
		goto exit;
	}
	
	
	exitcode = 0;
	
exit:
	if (mappingTable != NULL)	free(mappingTable);
	
    //deallocate each non-null entry in argtable[]
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	mappy_close(); //free all our calloc !
	
    return exitcode;
}