#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <mappy.h>
#include <argtable3.h>

#define FREEIMAGE_LIB
#include <FreeImage.h>

#include "utils\file_utils.h"
#include "utils\common.h"

struct arg_lit *help;
struct arg_file *file, *output;
struct arg_str *format, *chunk;
struct arg_end *end;

static void exportToBitmap(CHUNK *chunk, HDRSTR *header, short int colordepth , const char *filename)
{
	CHUNK *cmap;
	FIBITMAP *bitmap = NULL;
	int pitch = 4;
	
	if (colordepth == 8)
	{
		cmap =  mappy_getChunk( "CMAP" );
		if (cmap == NULL)
		{
			printf("error : color map not found.\n");
			return;
		}
	}
	
	
	FreeImage_Initialise(TRUE);
	
	if (colordepth == 8)
	{
		pitch = 1;
	}
	else if (colordepth < 24)
	{
		pitch = 2;
	}
	else if (colordepth < 32)
	{
		pitch = 3;
	}
	
	bitmap = FreeImage_ConvertFromRawBitsEx(TRUE, chunk->data, FIT_BITMAP, header->mapblockwidth, header->mapblockheight*header->mapnumblockgfx, pitch*header->mapblockwidth, colordepth, FI_RGBA_RED_MASK, FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK, TRUE);
	if (colordepth == 8)
	{
		RGBQUAD *pal = FreeImage_GetPalette(bitmap);
		int i;
		for (i = 0; i < 256; i++) {
			 pal[i].rgbRed 	= cmap->data[i*3 + 0];
			 pal[i].rgbGreen= cmap->data[i*3 + 1];
			 pal[i].rgbBlue = cmap->data[i*3 + 2];
		}
	}

	FreeImage_Save(FIF_BMP, bitmap, filename, BMP_DEFAULT);
 
	FreeImage_Unload(bitmap);
 
	FreeImage_DeInitialise();
}

int export(int argc, char *argv[])
{	
	CHUNK *rawChunk ;
	int exitcode = 0;
	int err;
	char *outputfilename = NULL;
	
	void *argtable[] = {
        help   = arg_litn(NULL, "help", 0, 1, "display this help"),
        file   = arg_file1("i", "input", "<infile>","input .FMP file"),
        output = arg_file0("o", "output", "<outfile>","output file"),
		chunk  = arg_str1(NULL, NULL,"<chunk_name>", "FMP chunk to export" ),
		format = arg_strn("f", "format","<format>", 0, 1, "export format (RAW|BMP)" ),
        end    = arg_end(ERROR_BUFFER_SIZE),
    };
	if (arg_nullcheck(argtable) != 0)
	{
		printf("error: insufficient memory\n");
		exitcode = -1;
		goto exit;
    }

	format->sval[0]="RAW";
    err = arg_parse(argc,argv,argtable);
	
    // special case: '--help' takes precedence over error reporting
    if (help->count > 0)
    {
        printf("Usage:\nEXPORT");
        arg_print_syntax(stdout, argtable, "\n");
        //printf("Demonstrate command-line parsing in argtable3.\n\n");
        arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		
        printf("\nValid chunks:\n");
        printf("  MPHD\textract header (formats : RAW)\n");
        printf("  ATHR\textract author info (formats : RAW)\n");
        printf("  CMAP\textract color map (formats : RAW)\n");
        printf("  BGFX\textract tiles (formats : RAW, BMP)\n");
        printf("  AGFX\textract alternate tiles (formats : RAW, BMP)\n");
        printf("  BKDT\textract block info(formats : RAW)\n");
        printf("  ANDT\textract anim info(formats : RAW)\n");
        printf("  BODY\textract layer 0, uncompressed (formats : RAW)\n");
        printf("  LAYx\textract layer x (1 to 7), uncompressed (formats : RAW)\n");
        //printf("  NLAY\textract layer x (formats : RAW)\n");
        printf("  xxxx\textract raw chunk (format : RAW)\n");
		
		exitcode = 0;
		goto exit;
    }
	
	
	// If the parser returned any errors then display them and exit
    if ((err > 0) || (argc == 1))
    {
        // print upto ERROR_BUFFER_SIZE error details contained in the arg_end struct
        arg_print_errors(stdout, end, "error");
        printf("Try 'EXPORT --help' for more information.\n");
		exitcode = -1;
		goto exit;
    }

		
	err = mappy_load( (char *)file->filename[0]);
	if (err != MER_NONE)	
	{
		err = mappy_getLastErrorID();
		if (err == MER_NOOPEN)
			printf("File not found : %s\n", (char *)file->filename[0]);
		else if (err == MER_OUTOFMEM)
			printf("Not enough memory to load %s\n", (char *)file->filename[0]);
		else //if (err == MER_MAPLOADERROR)
			printf("Error %d loading %s\n", err, (char *)file->filename[0]);
		exitcode = -1;
		goto exit;
	}
	
	rawChunk = mappy_getChunk( (char *)chunk->sval[0] );
	if (rawChunk == NULL)
	{
		printf("error : chunk %s not found.\n",chunk->sval[0]);
		exitcode = -1;
		goto exit;
	}
	
	outputfilename = (char *)output->filename[0];
	if (output->count == 0)	
	{
		outputfilename = calloc(FILENAME_MAX, 1);
		if (outputfilename == NULL)
		{
			printf("error : Not enough memory (%dbytes).\n",FILENAME_MAX);
			exitcode = -1;
			goto exit;
		}
	}
	
	
	if (strcmp("RAW", format->sval[0]) == 0)
	{
		if (output->count == 0)
		{
			char newExtension[16];
			sprintf(newExtension, ".%s.bin", chunk->sval[0] );
			
			file_getNewFilename((char *)file->filename[0], outputfilename, newExtension);
			
			//printf("Export to %s\n",outputfilename); 
		}
		
		FILE *o = fopen(outputfilename,"wb");
		if (!o)
		{
			printf("error : Invalid output filename %s.\n",outputfilename);
			exitcode = -1;
			goto exit;
		}
		
		fwrite(rawChunk->data, rawChunk->size, 1, o);
		fclose(o);
	}
	else if (strcmp("BMP", format->sval[0]) == 0)
	{
		HDRSTR *header = mappy_getHeader();
		
		if (output->count == 0)
		{
			char newExtension[16];
			sprintf(newExtension, ".%s.bmp", chunk->sval[0] );
			
			file_getNewFilename((char *)file->filename[0], outputfilename, newExtension);
			
			//printf("Export to %s\n",outputfilename); 
		}
		
		if (header == NULL)
		{
			printf("error : header not found.\n");
			exitcode = -1;
			goto exit;
		}
			
		if (strcmp("BGFX", chunk->sval[0]) == 0)
		{
			exportToBitmap(rawChunk, header, header->mapdepth, outputfilename);
		}
		else if (strcmp("AGFX", chunk->sval[0]) == 0)
		{
			exportToBitmap(rawChunk, header, 8, outputfilename);
		}
		else
		{
			printf("error : Can't extract chunk %s to bitmap.\n", chunk->sval[0]);
		}
	}
	else
	{
		printf("error : Invalid output format %s for chunk %s.\n", format->sval[0], chunk->sval[0]);
	}
	
	rawChunk = NULL;
	exitcode = 0;
	
exit:
	if ((output->count == 0) && (outputfilename != NULL))	{free(outputfilename);outputfilename = NULL;}
		
	mappy_close();
    // deallocate each non-null entry in argtable[]
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    return exitcode;
}