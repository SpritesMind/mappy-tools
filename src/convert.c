#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <mappy.h>
#include <argtable3.h>

#define FREEIMAGE_LIB
#include <FreeImage.h>

#include "utils\file_utils.h"
#include "utils\common.h"


	 
struct arg_lit *help;
struct arg_file *mapfile, *bmpfile, *output;
struct arg_int	*width, *height;
struct arg_str *format;
struct arg_end *end;

static HDRSTR *header;
static COLORSTR *colors;
static BLKSTR *blocks;
static signed short int *layer;
static unsigned char *gfx;

static int loadMap( const char *filename )
{
	long mapWidth, mapHeight, i;
	long dataSize = file_getSize( (char *)filename ); 
	long nbEntries;
	
	FILE *file = fopen(filename,"rb");
	if (file == NULL)
	{
		printf("error: can't open %s\n", filename);
		return 1;
	}
	
	//map size
	fread(&mapWidth,4,1,file);
	header->mapwidth = mapWidth;
	dataSize-=4;
	fread(&mapHeight,4,1,file);
	header->mapheight = mapHeight;
	dataSize-=4;
	
		
	
	//layer0
	nbEntries = header->mapwidth * header->mapheight;
	
	if (nbEntries > 0x7FFF)
	{
		printf("error: .FMP only support %d block, not %ld\n", 0x7FFF, nbEntries);
		fclose(file);
		return 1;
	}
	
	if (nbEntries != dataSize/4)
	{
		printf("error: invalid .MAP file (%ld entries expected vs %ld found)\n", nbEntries, dataSize/4);
		fclose(file);
		return 1;
	}

	layer = calloc(nbEntries, sizeof(signed short int));
	if (layer == NULL)
	{
		printf("error: can't allocate %ld bytes\n", nbEntries*sizeof(signed short int));
		fclose(file);
		return 1;
	}
	
	for(i=0; i < nbEntries; i++)
	{
		long entry;
		fread(&entry, 4, 1, file);
		layer[i] = entry & 0x7FFF;
	}
	fclose(file);
	return 0;
}


static int loadBitmap( const char *filename )
{
	int blockToImport, i;
	FIBITMAP *bitmap = FreeImage_Load(FIF_BMP, filename, BMP_DEFAULT);
	if (bitmap == NULL)
	{
		printf("error : Unable to load %s\n", filename);
		return 1;
	}
		
	int width = FreeImage_GetWidth(bitmap);
	int height = FreeImage_GetHeight(bitmap);
	int scan_width = FreeImage_GetPitch(bitmap);

	if (FreeImage_GetBPP(bitmap) != 8)
	{
		printf("error : %s depth should be %dbpp, not %dbpp\n", filename, header->mapdepth, FreeImage_GetBPP(bitmap));
		FreeImage_Unload(bitmap);
		return 1;
	}
	
	if ( (width % header->mapblockwidth) != 0 )
	{
		printf("error : %s width should be even multiple of %d\n", filename, header->mapblockwidth);
		FreeImage_Unload(bitmap);
		return 1;
	}
	if ( (height % header->mapblockheight) != 0 )
	{
		printf("error : %s height should be even multiple of %d\n", filename, header->mapblockheight);
		FreeImage_Unload(bitmap);
		return 1;
	}
	
	blockToImport = (width / header->mapblockwidth) * (height / header->mapblockheight);
	
	///blocks info (zeroed)
	header->mapnumblockstr = blockToImport;
	blocks = calloc (header->mapnumblockstr, sizeof(BLKSTR));
	for (i = 0; i< header->mapnumblockstr; i++)
	{
		BLKSTR *block = &blocks[i];
		block->bgoff = i;
	}
	
	///blocks gfx
	//create a blockWidth x (blockHeight*nbBlock) bitmap clone
	RGBQUAD c;
	c.rgbRed = 0;
	c.rgbGreen = 0;
	c.rgbBlue = 0;
	c.rgbReserved = 0; //index of pal to used as background color;

	FIBITMAP *tile = FreeImage_Copy(bitmap, 0, 0, header->mapblockwidth, header->mapblockheight);
	FIBITMAP *gfxBitmap = FreeImage_EnlargeCanvas(tile, 0, 0, 0, (blockToImport-1)*header->mapblockheight, &c, FI_COLOR_ALPHA_IS_INDEX);
	FreeImage_Unload(tile);
	//copy tile one by one
	for (i= 1; i < blockToImport; i++)
	{
		int left = (i*header->mapblockwidth) % width;
		int top = header->mapblockheight * ((i*header->mapblockwidth) / width);
		tile = FreeImage_CreateView(bitmap, left, top, left+header->mapblockwidth, top+header->mapblockheight); //CreateView is faster than CreateCopy according doc
		FreeImage_Paste(gfxBitmap, tile, 0, i*header->mapblockheight, 0xFFFF);
		FreeImage_Unload(tile);
	}
	
	//debug tester
	//FreeImage_Save(FIF_BMP, gfxBitmap, "debug.bmp", BMP_DEFAULT);
	
	
	//convert to rawBits
	width = FreeImage_GetWidth(gfxBitmap);
	height = FreeImage_GetHeight(gfxBitmap);
	scan_width = FreeImage_GetPitch(gfxBitmap);
	
	
	header->mapnumblockgfx = blockToImport;
	gfx = malloc(height * scan_width);
	FreeImage_ConvertToRawBits(gfx, gfxBitmap, scan_width, header->mapdepth, FI_RGBA_RED_MASK, FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK, TRUE);
	FreeImage_Unload(gfxBitmap);
	
	///CMAP
	RGBQUAD *pal = FreeImage_GetPalette(bitmap);
	if ( pal != NULL )
	{
		int i;
		int nbColors = min(256, FreeImage_GetColorsUsed(bitmap));
		colors = calloc(nbColors, sizeof(COLORSTR));
		for (i = 0; i < nbColors; i++) {
			colors[i].red = pal[i].rgbRed;
			colors[i].green = pal[i].rgbGreen;
			colors[i].blue = pal[i].rgbBlue;
		}
	}
	
	FreeImage_Unload(bitmap);
	
	
	return 0;
}

static int handleFormat( const char *format )
{	
	//TODO
	if ( strcmp(format, "LW4H4A4-1") != 0 )
	{
		printf("error : Custom format not implemented yet.\n");
		return 1;
	}

	return 0;
}

int convert(int argc, char *argv[])
{
	int exitcode = 0;
	int err;
	
	void *argtable[] = {
        help    = arg_litn(NULL, "help", 0, 1, "display this help"),
        mapfile = arg_file1("m", "map", "<infile>","input .MAP file"),
        bmpfile = arg_file0("b", "bitmap", "<infile>","input .BMP file"),
        width   = arg_int0("w", "tileW", "<value>","tile width in pixels (default 8)"),
        height  = arg_int0("h", "tileH","<value>","tile height in pixels (default 8)"),
        format  = arg_str0("f", "format", "<format>","MAP format (default LW4H4A4-1), not yet available"),
        output  = arg_file0("o", "output", "<outfile>","output .FMP 1.0 file"),
        end     = arg_end(ERROR_BUFFER_SIZE),
    };
	
	FreeImage_Initialise(TRUE);
	
	if (arg_nullcheck(argtable) != 0)
	{
		printf("error: insufficient memory\n");
		exitcode = -1;
		goto exit;
    }

	format->sval[0]="LW4H4A4-1";
	width->ival[0]=8;
	height->ival[0]=8;
	
	
    err = arg_parse(argc,argv,argtable);

    // special case: '--help' takes precedence over error reporting
    if (help->count > 0)
    {
        printf("Usage:\nCONVERT");
        arg_print_syntax(stdout, argtable, "\n");
        arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
    }
	
	
	// If the parser returned any errors then display them and exit
    if ((err > 0) || (argc == 1))
    {
        // print upto ERROR_BUFFER_SIZE error details contained in the arg_end struct
        arg_print_errors(stdout, end, "error");
        printf("Try 'CONVERT --help' for more information.\n");
		exitcode = -1;
		goto exit;
    }
	
	if (bmpfile->count == 0)	
	{
		bmpfile->filename[0] = calloc(FILENAME_MAX, 1);
		if (bmpfile->filename[0] == NULL)
		{
			printf("error : Not enough memory (%dbytes).\n",FILENAME_MAX);
			exitcode = -1;
			goto exit;
		}
		
		file_getNewFilename((char *)mapfile->filename[0],(char *) bmpfile->filename[0], ".bmp");
	}

	if (output->count == 0)	
	{
		output->filename[0] = calloc(FILENAME_MAX, 1);
		if (output->filename[0] == NULL)
		{
			printf("error : Not enough memory (%dbytes).\n",FILENAME_MAX);
			exitcode = -1;
			goto exit;
		}
		
		file_getNewFilename((char *)mapfile->filename[0], (char *)output->filename[0], ".fmp");
	}
	
	if (!file_isValid( (char *)mapfile->filename[0]))
	{
		printf("error : MAP file %s not found",  mapfile->filename[0]);
		exitcode = -1;
		goto exit;
	}
	if (!file_isValid( (char *)bmpfile->filename[0]))
	{
		printf("error : BMP file %s not found", bmpfile->filename[0]);
		exitcode = -1;
		goto exit;
	}
	
	header = (HDRSTR *) calloc(1, sizeof(HDRSTR)); //zeroed
	header->mapblockwidth = width->ival[0];
	header->mapblockheight = height->ival[0];
	header->mapdepth = 8; //TODO set by loadMap
	header->mapblockstrsize = header->mapblockwidth*header->mapblockheight; //need to set for futur calcul, before we call mappy_setHeader
	header->mapnumanimstr = 0;
	header->hasAlternateGFX=0;
	
	if ( handleFormat(format->sval[0]) != 0 )
	{
		exitcode = -1;
		goto exit;
	};
	
	//set header.mapwidth/header.height/layer0
	if ( loadMap( mapfile->filename[0] ) != 0)
	{
		exitcode = -1;
		goto exit;
	}
	
	//set header.numblock/block/blockGFX
	if ( loadBitmap( bmpfile->filename[0] ) != 0)
	{
		exitcode = -1;
		goto exit;
	}
	
	mappy_new( );
	mappy_setHeader(header);
	mappy_setColors(colors);
	mappy_setBlocks(blocks);
	mappy_setBlockGFX(gfx);
	mappy_setLayer(0, layer);
	
	err = mappy_save((char *) output->filename[0]);
	if (err != 0)	
	{
		err = mappy_getLastErrorID();
		if (err == MER_NOOPEN)
			printf("Unable to write to  %s\n", (char *) output->filename[0]);
		else if (err == MER_OUTOFMEM)
			printf("Not enough memory");
		else //if (err == MER_MAPLOADERROR)
			printf("Error %d writing to %s\n", err, (char *) output->filename[0]);
		exitcode = -1;
		goto exit;
	}
	
	
	exitcode = 0;
	
exit:
	FreeImage_DeInitialise();
	
    //deallocate each non-null entry in argtable[]
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	mappy_close(); //free all our calloc !
	
    return exitcode;
}