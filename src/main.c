#include <stdio.h>
#include <strings.h>
#include <mappy.h>
#include <argtable3.h>

#define ERROR_BUFFER_SIZE 4

extern int info(int argc, char *argv[]);
extern int export(int argc, char *argv[]);
extern int import(int argc, char *argv[]);
extern int create(int argc, char *argv[]);
extern int convert(int argc, char *argv[]);
extern int optimize(int argc, char *argv[]);

struct arg_lit *help, *version;
struct arg_str *command;
struct arg_end *end;


static void printver()
{
	printf("Mappy tools v0.0\n");
}

int main(int argc, char *argv[])
{
	int exitcode = 0;
	int err;

	void *argtable[] = {
        version = arg_litn(NULL, "version", 0, 1, "display version info"),
        help    = arg_litn(NULL, "help", 0, 1, "display this help"),
		command = arg_str1(NULL,NULL, "<command> [options]", "execute command" ),
        end     = arg_end(ERROR_BUFFER_SIZE),
    };
	if (arg_nullcheck(argtable) != 0)
	{
		printf("error: insufficient memory\n");
		exitcode = -1;
		goto exit;
    }

    err = arg_parse(argc,argv,argtable);
	if (command->count == 1)
	{
		if (strcmp("INFO", command->sval[0]) == 0)
		{
			exitcode = info(argc-1, argv+1);
			goto exit;
		}
		else if (strcmp("EXPORT", command->sval[0]) == 0)
		{
			exitcode = export(argc-1, argv+1);
			goto exit;
		}
		else if (strcmp("IMPORT", command->sval[0]) == 0)
		{
			exitcode = import(argc-1, argv+1);
			goto exit;
		}
		else if (strcmp("CREATE", command->sval[0]) == 0)
		{
			exitcode = create(argc-1, argv+1);
			goto exit;
		}
		else if (strcmp("CONVERT", command->sval[0]) == 0)
		{
			exitcode = convert(argc-1, argv+1);
			goto exit;
		}
		else if (strcmp("OPTIMIZE", command->sval[0]) == 0)
		{
			exitcode = optimize(argc-1, argv+1);
			goto exit;
		}
		
	}
    // special case: '--help' takes precedence over error reporting
    if (help->count > 0)
    {
		printver();
        printf("Usage:\nmappyCLI");
        arg_print_syntax(stdout, argtable, "\n");
        //printf("Demonstrate command-line parsing in argtable3.\n\n");
        arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		 printf("  <command> --help          display command help\n");
		
        printf("\nValid commands:\n");
        printf("  INFO\t\tget details about a Mappy file\n");
        printf("  EXPORT\textract data from a Mappy file\n");
        printf("  IMPORT\timport data to a Mappy file\n");
        printf("  CREATE\tcreate a Mappy from a bitmap\n");
        printf("  CONVERT\tconvert MAP to FMP\n");
		exitcode = 0;
		goto exit;
    }
	
	// special case 2
	if (version->count)
	{
		printver();
		exitcode = 0;
		goto exit;
	}
	
	// If the parser returned any errors then display them and exit
    if ((err > 0) || (argc == 1))
    {
        // print upto ERROR_BUFFER_SIZE error details contained in the arg_end struct
        arg_print_errors(stdout, end, "mappy tools");
        printf("Try '--help' for more information.\n");
		exitcode = -1;
		goto exit;
    }

exit:
    // deallocate each non-null entry in argtable[]
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    return exitcode;
}