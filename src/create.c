#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <mappy.h>
#include <argtable3.h>

#define FREEIMAGE_LIB
#include <FreeImage.h>

#include "utils\file_utils.h"
#include "utils\common.h"


	 
struct arg_lit *help, *duplicate, *extract;
struct arg_file *bmpfile, *output;
struct arg_int	*width, *height, *empty;
struct arg_end *end;

static HDRSTR *header;
static COLORSTR *colors;
static BLKSTR *blocks;
static signed short int *layer;
static unsigned char *gfx;



static int getDuplicate(FIBITMAP *bitmap, FIBITMAP *tile, int addedEmpty, int index )
{
	unsigned int tileIdx, i, j;
	unsigned char same = TRUE;
	//WARNING : works only with paletized bitmap
	for (tileIdx = 0; tileIdx < index+addedEmpty; tileIdx++)
	{
		FIBITMAP *existingTile = FreeImage_CreateView(bitmap, 0, tileIdx*header->mapblockheight, header->mapblockwidth, (tileIdx+1)*header->mapblockheight); //CreateView is faster than CreateCopy according doc
		same = TRUE;
		
		for(i = 0; i < header->mapblockheight; i++)
		{
			BYTE *bits_a = FreeImage_GetScanLine(tile, i); // first bitmap
			BYTE *bits_b = FreeImage_GetScanLine(existingTile, i); // second bitmap

			for (j = 0; j < header->mapblockwidth; j++)
			{
				if (bits_a[j] != bits_b[j]) 
				{
					same = FALSE;
					break;
				}
			}
			if (!same)	break;
		}
		
		FreeImage_Unload(existingTile);
			
		if (same)	return tileIdx;
	}
	
	return index;
}

static int loadBitmap( const char *filename, int addedEmpty, char removeDuplicate, char extractSheet )
{
	int blockToImport, uniqueBlock, i, j;
	FIBITMAP *bitmap = FreeImage_Load(FIF_BMP, filename, BMP_DEFAULT);
	if (bitmap == NULL)
	{
		printf("error : Unable to load %s\n", filename);
		return 1;
	}
		
	int width = FreeImage_GetWidth(bitmap);
	int height = FreeImage_GetHeight(bitmap);
	int scan_width = FreeImage_GetPitch(bitmap);

	if (FreeImage_GetBPP(bitmap) != 8)
	{
		printf("error : %s depth should be %dbpp, not %dbpp\n", filename, header->mapdepth, FreeImage_GetBPP(bitmap));
		FreeImage_Unload(bitmap);
		return 1;
	}
	
		
	header->mapwidth = width / header->mapblockwidth;
	if ( (width % header->mapblockwidth) != 0 )
	{
		printf("warning : %s width isn't even multiple of %d, crop needed\n", filename, header->mapblockwidth);
	}
	
	
	header->mapheight = height / header->mapblockheight;
	if ( (height % header->mapblockheight) != 0 )
	{
		printf("error : %s height isn't even multiple of %d, crop needed\n", filename, header->mapblockheight);
	}
	
	blockToImport = header->mapwidth * header->mapheight;
	

	
		
	
	///layer0
	if ((blockToImport+addedEmpty) > 0x7FFF)
	{
		printf("error: .FMP only support %d block, not %d\n", 0x7FFF, blockToImport+addedEmpty);
		FreeImage_Unload(bitmap);
		return 1;
	}

	layer = calloc(blockToImport, sizeof(signed short int));
	if (layer == NULL)
	{
		printf("error: can't allocate %d bytes\n", blockToImport*sizeof(signed short int));
		FreeImage_Unload(bitmap);
		return 1;
	}
	
	

	
	///blocks gfx
	//create a blockWidth x (blockHeight*nbBlock) bitmap clone
	RGBQUAD c;
	c.rgbRed = 0;
	c.rgbGreen = 0;
	c.rgbBlue = 0;
	c.rgbReserved = 0; //index of pal to used as background color;

	FIBITMAP *tile = FreeImage_Copy(bitmap, 0, 0, header->mapblockwidth, header->mapblockheight);
	FIBITMAP *gfxBitmap = FreeImage_EnlargeCanvas(tile, 0, 0, 0, (blockToImport+addedEmpty-1)*header->mapblockheight, &c, FI_COLOR_ALPHA_IS_INDEX);
	FreeImage_FillBackground(gfxBitmap, &c, FI_COLOR_ALPHA_IS_INDEX);
	
	//copy tile one by one
	uniqueBlock = blockToImport;
	j = 0;
	for (i= 0; i < blockToImport; i++)
	{
		int left = (i*header->mapblockwidth) % width;
		int top = header->mapblockheight * ((i*header->mapblockwidth) / width);
		
		FreeImage_Unload(tile);
		
		tile = FreeImage_CreateView(bitmap, left, top, left+header->mapblockwidth, top+header->mapblockheight); //CreateView is faster than CreateCopy according doc
		if (removeDuplicate)
		{
			int dupBlockID = getDuplicate(gfxBitmap, tile, addedEmpty, i );
			if (dupBlockID != i)
			{
				layer[i] = dupBlockID;
				uniqueBlock--;
				continue; //next for
			}		
		}
		layer[i] = j+addedEmpty;
		FreeImage_Paste(gfxBitmap, tile, 0, (j+addedEmpty)*header->mapblockheight, 0xFFFF);
		j++;
	}
	FreeImage_Unload(tile);
	
	
	//reduce height of gfxBitmap according uniqueBlock
	if (uniqueBlock != blockToImport)
	{
		printf("Removed %d duplicate tiles (%d vs %d)\n", blockToImport-uniqueBlock, uniqueBlock, blockToImport);
		FIBITMAP *tmpBitmap = FreeImage_EnlargeCanvas(gfxBitmap, 0, 0, 0, (uniqueBlock-blockToImport)*header->mapblockheight, &c, FI_COLOR_ALPHA_IS_INDEX);
		FreeImage_Unload(gfxBitmap);
		gfxBitmap = tmpBitmap;
	}
	
	
	
	//debug tester
	if (extractSheet)
	{
		char *sheetFilename = calloc(FILENAME_MAX, 1);
		if (sheetFilename == NULL)
		{
			printf("error : Not enough memory (%dbytes).\n",FILENAME_MAX);
			FreeImage_Unload(gfxBitmap);
			return 1;
		}
		
		file_getNewFilename( (char *)filename, (char *)sheetFilename, "_sheet.bmp");
		
		FreeImage_Save(FIF_BMP, gfxBitmap, sheetFilename, BMP_DEFAULT);
	}
	
	
	
	//convert to rawBits
	width = FreeImage_GetWidth(gfxBitmap);
	height = FreeImage_GetHeight(gfxBitmap);
	scan_width = FreeImage_GetPitch(gfxBitmap);
	
	
	header->mapnumblockgfx = uniqueBlock+addedEmpty;
	gfx = malloc(height * scan_width);
	FreeImage_ConvertToRawBits(gfx, gfxBitmap, scan_width, header->mapdepth, FI_RGBA_RED_MASK, FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK, TRUE);
	FreeImage_Unload(gfxBitmap);
	
		///blocks info (zeroed)
	header->mapnumblockstr = uniqueBlock+addedEmpty;
	blocks = calloc (header->mapnumblockstr, sizeof(BLKSTR));
	for (i = 0; i< header->mapnumblockstr; i++)
	{
		BLKSTR *block = &blocks[i];
		block->bgoff = i;
	}
	
	///CMAP
	RGBQUAD *pal = FreeImage_GetPalette(bitmap);
	if ( pal != NULL )
	{
		int i;
		int nbColors = min(256, FreeImage_GetColorsUsed(bitmap));
		colors = calloc(nbColors, sizeof(COLORSTR));
		for (i = 0; i < nbColors; i++) {
			colors[i].red = pal[i].rgbRed;
			colors[i].green = pal[i].rgbGreen;
			colors[i].blue = pal[i].rgbBlue;
		}
	}
	
	FreeImage_Unload(bitmap);	
	
	return 0;
}



int create(int argc, char *argv[])
{
	int exitcode = 0;
	int err;
	
	void *argtable[] = {
        help    = arg_litn(NULL, "help", 0, 1, "display this help"),
        bmpfile = arg_file1("i", "input", "<infile>","input .BMP file"),
        output  = arg_file0("o", "output", "<outfile>","output .FMP 1.0 file"),
        width   = arg_int0("w", "tileW", "<value>","tile width in pixels (default 8)"),
        height  = arg_int0("h", "tileH","<value>","tile height in pixels (default 8)"),
        duplicate = arg_litn(NULL, "remove-duplicate", 0, 1, "remove duplicate tile"),
        extract  = arg_litn(NULL, "extract", 0, 1, "extract sheet"),
        empty   = arg_int0("e", "empty","<value>","empty tile to add at start (default 1)"),
        end     = arg_end(ERROR_BUFFER_SIZE),
    };
	
	FreeImage_Initialise(TRUE);
	
	if (arg_nullcheck(argtable) != 0)
	{
		printf("error: insufficient memory\n");
		exitcode = -1;
		goto exit;
    }

	width->ival[0]=8;
	height->ival[0]=8;
	empty->ival[0]=1;
	
	
    err = arg_parse(argc,argv,argtable);

    // special case: '--help' takes precedence over error reporting
    if (help->count > 0)
    {
        printf("Usage:\nCREATE");
        arg_print_syntax(stdout, argtable, "\n");
        arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
    }
	
	
	// If the parser returned any errors then display them and exit
    if ((err > 0) || (argc == 1))
    {
        // print upto ERROR_BUFFER_SIZE error details contained in the arg_end struct
        arg_print_errors(stdout, end, "error");
        printf("Try 'CREATE --help' for more information.\n");
		exitcode = -1;
		goto exit;
    }
	
	if (output->count == 0)	
	{
		output->filename[0] = calloc(FILENAME_MAX, 1);
		if (output->filename[0] == NULL)
		{
			printf("error : Not enough memory (%dbytes).\n",FILENAME_MAX);
			exitcode = -1;
			goto exit;
		}
		
		file_getNewFilename((char *)bmpfile->filename[0], (char *)output->filename[0], ".fmp");
	}
	
	if (!file_isValid( (char *)bmpfile->filename[0]))
	{
		printf("error : BMP file %s not found", bmpfile->filename[0]);
		exitcode = -1;
		goto exit;
	}
	
	header = (HDRSTR *) calloc(1, sizeof(HDRSTR)); //zeroed
	header->mapblockwidth = width->ival[0];
	header->mapblockheight = height->ival[0];
	header->mapdepth = 8; //TODO set by loadMap
	header->mapblockstrsize = header->mapblockwidth*header->mapblockheight; //need to set for futur calcul, before we call mappy_setHeader
	header->mapnumanimstr = 0;
	header->hasAlternateGFX=0;
	
	
	if ( loadBitmap( bmpfile->filename[0], empty->ival[0], (duplicate->count ==1), (extract->count ==1) ) != 0)
	{
		exitcode = -1;
		goto exit;
	}
	
	mappy_new( );
	mappy_setHeader(header);
	mappy_setColors(colors);
	mappy_setBlocks(blocks);
	mappy_setBlockGFX(gfx);
	mappy_setLayer(0, layer);
	
	err = mappy_save((char *) output->filename[0]);
	if (err != 0)	
	{
		err = mappy_getLastErrorID();
		if (err == MER_NOOPEN)
			printf("Unable to write to  %s\n", (char *) output->filename[0]);
		else if (err == MER_OUTOFMEM)
			printf("Not enough memory");
		else //if (err == MER_MAPLOADERROR)
			printf("Error %d writing to %s\n", err, (char *) output->filename[0]);
		exitcode = -1;
		goto exit;
	}
	
	
	exitcode = 0;
	
exit:
	FreeImage_DeInitialise();
	
    //deallocate each non-null entry in argtable[]
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	mappy_close(); //free all our calloc !
	
    return exitcode;
}