#include <stdio.h>
#include <strings.h>
#include <mappy.h>
#include <argtable3.h>

#include "utils/common.h"


struct arg_lit *verb, *full, *help;
struct arg_file *file;
struct arg_end *end;

int info(int argc, char *argv[])
{
	HDRSTR *header;
	ATHSTR *author;
	COLORSTR *colors;
	BLKSTR *blocks;
    ANMSTR (*animations)[];
	signed short int *layer;
	
	int exitcode = 0;
	int i,j,k;
	int err;
	
	void *argtable[] = {
        help    = arg_litn(NULL, "help", 0, 1, "display this help"),
        file    = arg_file1("i", "input", "<infile>","input .FMP file"),
        verb    = arg_litn(NULL, "verbose", 0, 1, "verbose output"),
        full    = arg_litn(NULL, "full", 0, 1, "full output"),
        end     = arg_end(ERROR_BUFFER_SIZE),
    };
	if (arg_nullcheck(argtable) != 0)
	{
		printf("error: insufficient memory\n");
		exitcode = -1;
		goto exit;
    }

    err = arg_parse(argc,argv,argtable);

    // special case: '--help' takes precedence over error reporting
    if (help->count > 0)
    {
        printf("Usage:\nINFO");
        arg_print_syntax(stdout, argtable, "\n");
        //printf("Demonstrate command-line parsing in argtable3.\n\n");
        arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
    }
	
	
	// If the parser returned any errors then display them and exit
    if ((err > 0) || (argc == 1))
    {
        // print upto ERROR_BUFFER_SIZE error details contained in the arg_end struct
        arg_print_errors(stdout, end, "error");
        printf("Try 'INFO --help' for more information.\n");
		exitcode = -1;
		goto exit;
    }

		
	err = mappy_load( (char *)file->filename[0]);
	if (err != MER_NONE)	
	{
		err = mappy_getLastErrorID();
		if (err == MER_NOOPEN)
			printf("File not found : %s\n", (char *)file->filename[0]);
		else if (err == MER_OUTOFMEM)
			printf("Not enough memory to load %s\n", (char *)file->filename[0]);
		else //if (err == MER_MAPLOADERROR)
			printf("Error %d loading %s\n", err, (char *)file->filename[0]);
		exitcode = -1;
		goto exit;
	}
	
	printf("\nMappy info\n");
	printf("Map : \t\t%s\n", (char *)file->filename[0]);
	
	author = mappy_getAuthorInfo();
	if (author != NULL)
	{
		printf("Author: \t%s\n", (author->name[0]==0)?"-":author->name);
		printf("Info: \t\t%s\n", (author->info1[0]==0)?"-":author->info1);
		printf("\t\t%s\n", (author->info2[0]==0)?"-":author->info2);
		printf("\t\t%s\n", (author->info3[0]==0)?"-":author->info3);
	}
	
	header = mappy_getHeader();
	if (header != NULL)
	{
		printf("Map type: \t%d (%s)\n", header->maptype, typeName[header->maptype]);
		printf("Size: \t\t%dx%d blocks\n", header->mapwidth, header->mapheight);
		printf("Block: \t\t%dx%d pixels\n", header->mapblockwidth, header->mapblockheight);
		printf("Depth: \t\t%dbits\n", header->mapdepth);
		//printf("Data size: \t%dbytes/block\n", header->mapblockstrsize);
		printf("Num. of blocks: %d\n", header->mapnumblockstr);
		printf("Num. of gfx: \t%d\n", header->mapnumblockgfx);
		printf("GFX size: \t%ld bytes\n", mappy_getBlockGFXSize());
		printf("Num. of anim: \t%d\n", header->mapnumanimstr);
		if (header->hasAlternateGFX)
			printf("8bit GFX available (%ld bytes)\n", mappy_getAlternateGFXSize());
	}
	
	if (full->count)
	{
		colors = mappy_getColors();
		if (colors != NULL)
		{
			for (i = 0; i < 256; i++)
			{
				COLORSTR color = colors[i];
				printf("Color %3d:\t0x%.2x%.2x%.2x (%3d,%3d,%3d)\n", i, color.red, color.green, color.blue, color.red, color.green, color.blue);
			}
		}
		
		blocks = mappy_getBlocks();
		if (blocks != NULL)
		{
			for (i = 0; i < header->mapnumblockstr; i++)
			{
				BLKSTR* block =&blocks[i];
				printf("Block %3d is based on tile %ld %s\n", i, block->bgoff, (block->bgoff==0)?"(empty block ?)":"" );
				//no more data printed but could easily be done
			}
		}
		
		animations = mappy_getAnimations();
		if (animations != NULL)
		{
			for (i = 0; i < header->mapnumanimstr; i++)
			{
				ANMSTR anim =(*animations)[i];
				printf("Anim %3d is based on %2d blocks ( ", i, anim.frameCount);
				for (j=0; j < anim.frameCount; j++)
				{
					printf("%d ", anim.blocks[j]);
				}
				printf(")\n");
			}
		}
		
		for (k = 0; k < 100; k++)
		{
			layer = mappy_getLayer(k);
			if (layer != NULL)
			{
				printf("\nLayer %d extract:", k);
				for (i = 0; i < 12; i++)
				{
					printf("\n");
					if ( i >= header->mapheight)	break;
					
					for (j = 0; j< 12; j++)
					{
						if ( j >= header->mapwidth)	break;
						printf("%4d ", layer[ j + header->mapwidth*i ]);
					}
				}
				printf("\n");
			}
			else
				break;
		}
		
	}

	
	exitcode = 0;
	
exit:
    //deallocate each non-null entry in argtable[]
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	mappy_close();
    return exitcode;
}